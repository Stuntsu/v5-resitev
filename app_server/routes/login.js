var express = require('express');
var router = express.Router();
var ctrlLogin = require('../controllers/login')

/* GET login page. */
router.get('/', ctrlLogin.login);
router.post('/', ctrlLogin.checkLogin)

module.exports = router;
