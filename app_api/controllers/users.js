var mongoose = require('mongoose');
var User = mongoose.model('User');

var returnJson = function(res, status, content) {
  res.status(status);
  res.json(content);
};

module.exports.userExists = function(req, res) {
    User.findOne({"username": req.params.username, "password": req.params.password})
    .exec(function(err, user) {
        var exists = (user ? true : false);
        returnJson(res, 200, {"exists": exists});
    });
};