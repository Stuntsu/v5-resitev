var express = require('express');
var router = express.Router();
var ctrlUsers = require('../controllers/users');
var ctrlFiles = require('../controllers/files');

router.get('/user/:username/:password', ctrlUsers.userExists);
router.get('/file/:filename/:type', ctrlFiles.getFile);
router.post('/file', ctrlFiles.saveFile);
router.delete('/file', ctrlFiles.deleteFile);
router.get('/files', ctrlFiles.listFiles);

module.exports = router;